#!/bin/sh

PYPY_OLD=$1
PYPY_NEW=$2

PYPY_OLD_BASE=$( basename "$PYPY_OLD" )
PYPY_NEW_BASE="pypy-new"

# CHECKOUT BENCHMARK SUITES
echo "Checkout benchmark suites"
if [ ! -d "pypy-benchmarks" ]; then
    echo "> cloning pypy-benchmarks"
    hg clone https://bitbucket.org/pypy/benchmarks pypy-benchmarks
else
    echo "> pypy-benchmarks already cloned"
fi
if [ ! -d "memory-benchmarks" ]; then
    echo "> cloning memory-benchmarks"
    hg clone https://bitbucket.org/cfbolz/python-memory-benchmarks memory-benchmarks
else
    echo "> memory-benchmarks already cloned"
fi

# CREATE VIRTUALENV
echo "Creating virtual environment"
if [ ! -d "pypyenv" ]; then
    python memory-benchmarks/pypa-virtualenv-8dddd68/virtualenv.py -p $PYPY_OLD pypyenv
    cp $PYPY_NEW pypyenv/bin/pypy-new

    # Dependency: antlr
    wget https://bitbucket.org/l.diekmann/storage-strategy-benchmarks/downloads/antlr-3.1.3.tar.gz
    tar xzf antlr-3.1.3.tar.gz

    # Dependency: trained data for disaster
    wget https://bitbucket.org/l.diekmann/storage-strategy-benchmarks/downloads/disaster_trained.tar.gz
    tar xfz disaster_trained.tar.gz
    mv trained/ memory-benchmarks/disaster/disaster/data/chunker/tagr01/

    # Install dependencies in virtualenv
    . pypyenv/bin/activate
    pip install psutil
    pip install pyyaml
    pip install sqlalchemy
    cd antlr_python_runtime-3.1.3/
    python setup.py install
    cd -

    cd memory-benchmarks/disaster
    bunzip2 c1-una_small.xml.bz2
    cd -
    rm disaster_trained.tar.gz
    rm antlr-3.1.3.tar.gz

    # run pypy-interp/translate as a normal process once (otherwise it won't work in a subprocess later)
    . pypyenv/bin/activate
    cd memory-benchmarks
    PYTHONPATH=. $PYPY_OLD_BASE benchmarks/pypy-interp.py
    PYTHONPATH=. $PYPY_OLD_BASE benchmarks/pypy-translate.py
    cd -
else
    echo "> already created"
fi

# RUN BENCHMARKS
echo "Running storage strategy benchmarks..."
if [ ! -d "memory-benchmarks/csv" ]; then
    mkdir memory-benchmarks/csv
fi
./benchmarks.sh --memory $PYPY_OLD_BASE speed
./benchmarks.sh --memory $PYPY_NEW_BASE speed

echo "Running PyPy benchmarks..."
./benchmarks.sh --pypy $PYPY_OLD_BASE
./benchmarks.sh --pypy $PYPY_NEW_BASE

# GENERATE COMPARISON TABLE
echo "Generating html table"
python scripts/write_latextable.py all-speed html $PYPY_OLD_BASE,$PYPY_NEW_BASE
