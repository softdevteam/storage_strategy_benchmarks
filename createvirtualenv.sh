#!/bin/sh

# create pypyenv
echo "CREATE PYPY VIRTUALENV"
python memory-benchmarks/pypa-virtualenv-8dddd68/virtualenv.py -p pypy-all/pypy-all pypyenv

# copy over pypy versions to pypyenv
echo "COPY PYPY VERSIONS TO PYPYENV"
cp builds/pypy-* pypyenv/bin/

# download antlr
echo "DOWNLOAD ANTLR"
wget http://www.antlr3.org/download/Python/antlr_python_runtime-3.1.3.tar.gz
tar xzf antlr_python_runtime-3.1.3.tar.gz

# install dependencies in pypyenv
echo "INSTALL DEPENDENCIES IN PYPYENV"
. pypyenv/bin/activate
pip install psutil
pip install pyyaml
pip install sqlalchemy
cd antlr_python_runtime-3.1.3/
python setup.py install
cd -

# run pypy-interp/translate as a normal process once (fix to make it run in a subprocess later)
. pypyenv/bin/activate
cd memory-benchmarks
PYTHONPATH=. pypy-none benchmarks/pypy-interp.py
PYTHONPATH=. pypy-none benchmarks/pypy-translate.py
