INTRODUCTIONS
-------------

These scripts can be used to reproduce the results from the paper "Storage
strategies for collections in dynamically typed languages" by Bolz, Diekmann,
and Tratt.

Beware that a total build and benchmark run will take approximately 16 hours
to complete on a typical machine (4 hours translation, 12 hours running all
benchmarks).

Dependencies:
gnuplot mercurial wget graphviz gcc make python-dev libffi-dev libsqlite3-dev
pkg-config libz-dev libbz2-dev libncurses-dev libexpat1-dev libssl-dev
libgc-dev python-sphinx python-greenlet

To build and run the experiments, simply run "make":

  $ make

If your machine has at least 16Gb of RAM, you can run "make -j 2" to speed up
the build process. After the experiments have run, the results can be found
as follows:

PyPy versions:
    pypyenv/bin/pypy-all        # PyPy with all     optimizations
    pypyenv/bin/pypy-none       # PyPy with no      optimizations
    pypyenv/bin/pypy-list       # PyPy with list    optimizations only
    pypyenv/bin/pypy-set        # PyPy with set     optimizations only
    pypyenv/bin/pypy-dict       # PyPy with dict    optimizations only
    pypyenv/bin/pypy-int        # PyPy with integer optimizations only
    pypyenv/bin/pypy-string     # PyPy with string  optimizations only
    pypyenv/bin/pypy-float      # PyPy with float   optimizations only

Diagrams:
    diagrams/transitions.pdf    # Diagram showing transitions between strategies and dehomogenisation

Tables (Latex files):
    tables/osheapsize.tex                  # Memory results of paper benchmarks
    tables/all_speed_pypy_percentages.tex  # Execution times relative to pypy-none
    tables/all_speed_pypy.tex              # Exeuction times of all benchmarks

PyPy Sourcecode:

The sourcecode of PyPy can be found in standard/pypy-src after make has been
executed. The sourcecode is also available online at
https://bitbucket.org/pypy/pypy or can be manually checked out by running:

$ hg clone https://bitbucket.org/pypy/pypy

The relevant changes described in the paper can be found at:

- pypy/objspace/std/listobject.py
- pypy/objspace/std/setobject.py
- pypy/objspace/std/dictmultiobject.py
- pypy/objspace/std/objspace.py
