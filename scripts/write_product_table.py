import sys
import reader
import math

quantile = 1.959963984540

BM_MAP = {"networkx" : "NetworkX", "scapy" : "Scapy", "pyexcelerator" : "PyExcelerator",
  "pydblite" : "PyDbLite", "pypy-interp" : "PyPy-interp", "pypy-translate" : "PyPy-translate",
  "whoosh" : "whoosh", "feedparser" : "Feedparser", "nltk-wordassoc" : "nltk-wordassoc",
  "disaster" : "disaster", "multiwords" : "multiwords", "orm" : "orm",
  "findprimes" : "findprimes", "invindex" : "invindex", "slowsets" : "slowsets",
  "liststrategy" : "liststrategy*", "setstrategy" : "setstrategy*",
  "dictstrategy" : "dictstrategy*", "richards" : "richards", "slowspitfire" : "slowspitfire",
  "django" : "django", "spambayes" : "spambayes", "html5lib" : "html5lib", "ai" : "ai",
  "raytrace-simple" : "raytrace-simple", "crypto_pyaes" : "crypto\\_pyaes", "go" : "go",
  "chaos" : "chaos", "twisted_names" : "twisted\\_names", "meteor-contest" : "meteor-contest",
  "pyflate-fast" : "pyflate-fast", "telco" : "telco", "genshi_text" : "genshi\\_text", "nbody_modified" : "nbody\\_modified",
  "spectral-norm" : "spectral-norm", "sympy_integrate" : "sympy\\_integrate",
  "fannkuch" : "fannkuch", "bm_chameleon" : "bm\\_chameleon", "bm_mako" : "bm\\_mako"
}

def format_float(f):
    return "%0.3f" % f

def std_dev_product((A, std_dev_A), (B, std_dev_B)):
    return A * B, math.sqrt((std_dev_A / A) ** 2 + (std_dev_B / B) ** 2) * A * B

def std_dev_quotient((A, std_dev_A), (B, std_dev_B)):
    ratio = A / B
    return ratio, math.sqrt((std_dev_A / A) ** 2 + (std_dev_B / B) ** 2) * ratio

def std_dev_power((A, std_dev_A), exponent): # assume fixed exponent
    return A ** exponent, exponent * std_dev_A * A ** (exponent - 1)

def comparison_indicator((A, std_dev_A), (B, std_dev_B)):
    if abs(A - B) < (std_dev_A + std_dev_B) * quantile:
        return "="
    if A < B:
        return "<"
    else:
        return ">"

def product(bench, *pythons):
    if bench is None:
        def get_data(python, bench):
            return geomean[python]
    else:
        def get_data(python, bench):
            return all_data[python, bench]
    curr = get_data(pythons[0], bench)
    for p in pythons[1:]:
        curr = std_dev_product(curr, get_data(p, bench))
    return curr


def format_with_conf((value, stddev)):
    return u"%0.3f $\pm$ %0.3f" % (value, stddev * quantile)



def main():
    executables = sys.argv[1].split(",") #["pypy-all", "pypy-none"]
    benchmarks = reader.benchmarks
    benchmarks.remove('empty')
    benchmarks.remove('findprimes')
    benchmarks.remove('liststrategy')
    benchmarks.remove('setstrategy')
    benchmarks.remove('dictstrategy')
    benchmarks.remove('pypy-interp')
    benchmarks.remove('pypy-translate')
    benchmarks.sort()
    speed = reader.read_speed(executables)
    pypyspeed = reader.read_pypy_speed(executables)

    pypy_benchmarks = "richards,slowspitfire,django,spambayes,html5lib,ai,raytrace-simple,crypto_pyaes,go,chaos,twisted_names,meteor-contest,pyflate-fast,telco,genshi_text,nbody_modified,spectral-norm,sympy_integrate,fannkuch,bm_chameleon,bm_mako".split(",")
    pypy_benchmarks.sort()
    benchmarks += pypy_benchmarks

    global all_data, geomean
    all_data = {}
    speedup = {}
    stddev_speedup = {}
    geomean = {}
    for e in executables:
        speedup[e] = (1.0, 0.0)

    for e in executables:
        speed[e].update(pypyspeed[e])

    for b in benchmarks:
        for e in executables:
            value = float(speed[e][b]['avg'])
            normalize_to = float(speed['pypy-none'][b]['avg'])
            std_dev_A = float(speed[e][b]['err']) / quantile # undo confidence interval
            std_dev_B = float(speed['pypy-none'][b]['err']) / quantile
            ratio = all_data[e, b] = std_dev_quotient((value, std_dev_A), (normalize_to, std_dev_B))
            speedup[e] = std_dev_product(speedup[e], ratio)
    for e in executables:
        geomean[e] = std_dev_power(speedup[e], 1.0 / len(benchmarks))

    columns = [("pypy-all",), ("pypy-list", "pypy-dict", "pypy-set"), ("pypy-ints", "pypy-strings", "pypy-floats")]

    f = open("latex/sanity_check.tex", "w")
    f.write("\\addtolength{\\tabcolsep}{-2pt}\n")
    f.write("\\begin{tabular}")
    # column alignment
    f.write("{ l >{\\hspace{6pt}}rcl >{\\hspace{6pt}}rcl >{\\hspace{6pt}}rcl } ")
    f.write("\\toprule\n")

    # executable row
    f.write("\emph{Benchmark}")
    prefix = "~~~"
    for i in range(3):
        for e in columns:
            if i < len(e):
                value = prefix + e[i]
            else:
                value = ""
            f.write(" & \multicolumn{3}{l}{\hspace{5pt}%s\hspace{5pt}}" % (value, ))
        prefix = "* "
        f.write("\\\\\n")
    f.write("\\midrule\n")

    formatstring = "$\\mathbf{%s}$~~%0.3f & \hspace{-6pt}\\tiny{$\pm$} & \hspace{-6pt}\\tiny{%0.3f}"
    # benchmark rows and results
    for b in benchmarks:
        if b == "ai":
            f.write("\\midrule\n")

        b_escape = b.replace('_','\_')
        f.write(BM_MAP[b])
        comparison = None
        for c in columns:
            value = product(b, *c)
            if comparison is None:
                comparison = value
                extra = ""
            else:
                extra = comparison_indicator(comparison, value)
            f.write(" & ")
            f.write(formatstring % (extra, value[0], value[1] * quantile))
        f.write(" \\\\\n")

    f.write("\\midrule\n")

    # write speedup row
    s = "Combined Ratios"
    f.write("\emph{%s}" % (s,))

    comparison = None
    for c in columns:
        value = product(None, *c)
        if comparison is None:
            comparison = value
            extra = ""
        else:
            extra = comparison_indicator(comparison, value)
        f.write(" & ")
        f.write(formatstring % (extra, value[0], value[1]))

    f.write(" \\\\\n")
    f.write("\\bottomrule\n")
    f.write("""
\end{tabular}
""")
    f.close()

if __name__ == '__main__':
    main()
