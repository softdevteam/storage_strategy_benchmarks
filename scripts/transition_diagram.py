import sys
from pprint import pprint

proper_names = {
    'class_dict': 'ClassDictStrategy',
    'instance_dict': 'InstanceDictStrategy',
    'module_dict': 'ModuleDictStrategy',
    'kwargs_dict': 'KwargsDictStrategy',
    'identity_dict': 'IdentityDictStrategy',
    'int_dict': 'IntegerDictStrategy',
    'string_dict': 'StringDictStrategy',
    'object_dict': 'ObjectDictStrategy',
    'empty_dict': 'EmptyDictStrategy',

    'range_list': 'RangeListStrategy',
    'int_list': 'IntegerListStrategy',
    'string_list': 'StringListStrategy',
    'float_list': 'FloatListStrategy',
    'object_list': 'ObjectListStrategy',
    'empty_list': 'EmptyListStrategy',

    'object_set': 'ObjectSetStrategy',
    'string_set': 'StringSetStrategy',
    'int_set': 'IntegerSetStrategy',
    'empty_set': 'EmptySetStrategy',
}

all_keys = {
    'empty_dict to identity_dict',
    'empty_dict to int_dict',
    'empty_dict to string_dict',
    'empty_dict to object_dict',

    'class_dict to object_dict',
    'instance_dict to object_dict',
    'module_dict to object_dict',
    'kwargs_dict to object_dict',
    'identity_dict to object_dict',
    'int_dict to object_dict',
    'string_dict to object_dict',

    'identity_dict to empty_dict',
    'int_dict to empty_dict',
    'string_dict to empty_dict',
    'object_dict to empty_dict',
    'class_dict to empty_dict',
    'instance_dict to empty_dict',
    'module_dict to empty_dict',
    'kwargs_dict to empty_dict',
    'new empty_dict',
    'new class_dict',
    'new instance_dict',
    'new kwargs_dict',
    'new module_dict',
    'new string_dict',

    'empty_list to int_list',
    'empty_list to object_list',
    'empty_list to string_list',
    'empty_list to float_list',
    'range_list to object_list',
    'int_list to object_list',
    'string_list to object_list',
    'float_list to object_list',
    'range_list to empty_list',
    'int_list to empty_list',
    'string_list to empty_list',
    'float_list to empty_list',
    'object_list to empty_list',
    'range_list to int_list',
    'new empty_list',
    'new int_list',
    'new string_list',
    'new range_list',
    'new object_list',

    'empty_set to object_set',
    'empty_set to string_set',
    'empty_set to int_set',
    'string_set to object_set',
    'int_set to object_set',
    'object_set to empty_set',
    'string_set to empty_set',
    'int_set to empty_set',
    'new empty_set',
    'new string_set',
    'new object_set',
    'new int_set',
}

expensive_transitions = {
    'class_dict to object_dict',
    'instance_dict to object_dict',
    'module_dict to object_dict',
    'kwargs_dict to object_dict',
    'identity_dict to object_dict',
    'int_dict to object_dict',
    'string_dict to object_dict',
    'range_list to object_list',
    'int_list to object_list',
    'string_list to object_list',
    'float_list to object_list',
    'range_list to int_list',
    'string_set to object_set',
    'int_set to object_set',
}


def median(d):
    l = []
    for size, count in d.iteritems():
        l.extend([size] * count)
    l.sort()
    return l[len(l) // 2]

def average(d):
    l = []
    for size, count in d.iteritems():
        l.extend([size] * count)
    l.sort()
    return sum(l) / float(len(l))

def fmt_average(name, d):
    a = average(d)
    if "empty" in name:
        return ""
    return "\\n(avg#: %0.2f)" % (a, )

def fmt_count(d):
    r = u"{0:,}".format(sum(d.values()))
    return r.replace(u",", u"\u202f")


def read_and_join(fns):
    if len(fns) == 0:
        from reader import benchmarks
        fns = ["memory-benchmarks/csv/transitions_pypy-transitions_%s.csv" % b
                    for b in benchmarks
                    if "strategy" not in b and b != "empty" and b != "findprimes"]

    all_transitions = {}
    all_codesites = {}
    total_size1 = total_size2 = 0

    for fn in fns:
        try:
            with file(fn) as f:
                s = f.read()
        except IOError:
            continue
        s = s.replace("str_list", "string_list") # :-(
        glob = {}
        exec s in glob
        size1 = size2 = 0
        for name, items in glob["strategytransitions"].iteritems():
            total = all_transitions.setdefault(name, {})
            for size, count in items.iteritems():
                total[size] = total.get(size, 0) + count
                size1 += count
                total_size1 += count
        if glob.has_key("codesites"):
            for name, items in glob["codesites"].iteritems():
                total = all_codesites.setdefault(name, {})
                for event, count in items.iteritems():
                    total[event] = total.get(event, 0) + count
                    size2 += count
                    total_size2 += count
            assert size1 == size2
    if all_codesites != {}:
        assert total_size1 == total_size2
    return all_transitions, all_codesites


def print_graph(transitions, restrict):

    nodes_seen = set()
    output = []

    output.append(u"digraph G{")
    output.append(u"    rankdir=LR;")
    for name, real_name in sorted(proper_names.items()):
        if restrict not in name:
            continue
        output.append(u"    %s [label=%s, shape=box];" % (name, real_name))
    for name, value in transitions.iteritems():
	if name == "new string" or name == "new unicode":
            continue
        if restrict not in name:
            continue
        nodes_seen.add(name)
        extra = ""
        if name.startswith("new "):
            strategy_name = name[len("new "):]
            output.append(u'    %s_new [label = " ", shape=none]' % (strategy_name, ))
            from_ = strategy_name + "_new"
            to = strategy_name
        else:
            assert " to " in name
            from_, to = name.split(" to ")
            if from_ == to:
                continue
            if to == "empty_dict":
                extra = ",constraint=false"
        count = fmt_count(value)
        avg = fmt_average(name, value)
        color = ''
        if name in expensive_transitions:
            color = ", color=red"
        output.append(u'    %s -> %s [label = "%s%s"%s%s]' % (from_, to, count, avg, color, extra))

    for name in all_keys:
        if restrict not in name:
            continue
        if name in nodes_seen:
            continue
        if name.startswith("new "):
            name = name[len("new "):]
            output.append(u'    %s_new [label = " ", shape=none]' % (name, ))
            output.append(u'    %s_new -> %s [color=gray]' % (name, name))
        else:
            if "to empty" in name:
                continue
            a, b = name.split(" to ")
            output.append(u'    %s -> %s [color=gray]' % (a, b))

    output.append(u"}")
    print "\n".join(output).encode("utf-8")



def main():
    import argparse

    parser = argparse.ArgumentParser(description='Make a dot file')
    parser.add_argument('files', metavar='FILE', type=str, nargs='*',
                       help='all transition files to sum over')
    parser.add_argument('--restrict', help='restrict to one kind of type', default="")

    args = parser.parse_args()

    files = args.files

    transitions, codesites = read_and_join(files)
    print_graph(transitions, args.restrict)
    #print_blub(transitions, codesites)


if __name__ == '__main__':


    main()
