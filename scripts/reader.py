import math
import json

#executables = ["python", "pypy-none", "pypy", "pypy-32-setonly", "pypy-32-dictsonly", "pypy-32-attrsonly", "pypy-32-all"]
executables = ["pypy-none", "pypy-all"]
analysis = "gclogs"
csv_folder = "memory-benchmarks/csv"

benchmarks = ["networkx", "scapy", "pyexcelerator",
                  "pydblite", "whoosh","findprimes",
                  "pypy-interp", "pypy-translate",
                  "feedparser", "nltk-wordassoc", "disaster",
                  "multiwords", "orm",
                  "invindex", "slowsets", "liststrategy", "setstrategy", "dictstrategy", "empty"]

def read_gclogs(executables=executables):
    analysis = "gclogs"
    data = {}.fromkeys(executables)
    for e in executables:
        print "Executable:", e
        bench_data = {}
        for b in benchmarks:
            filename = "%s/%s_%s_%s.csv" % (csv_folder, analysis, e, b)
            print "Fetching %s of %s from %s" % (b, e, filename)
            try:
                f = open(filename, 'r')
                #import pdb;pdb.set_trace()
            except IOError:
                print "===> No such file. Skipped!"
                continue

            all_full = [int(l) for l in f.readlines()]
            if all_full == [] or all_full is None:
                print "===> Empty data. Skipped!"
                continue

            avg_full = float(sum(all_full)) / len(all_full)

            error = calculate_error(all_full, avg_full)

            bench_data[b] = {}
            bench_data[b]["avg"] = avg_full / 1024**2
            bench_data[b]["err"] = error / 1024**2

            f.close()
        data[e] = bench_data
    return data

def read_osheapsize(executables=executables):
    analysis = "osheapsize"
    data = {}.fromkeys(executables)
    for e in executables:
        print "Executable:", e
        bench_data = {}
        for b in benchmarks:
            filename = "%s/%s_%s_%s.csv" % (csv_folder, analysis, e, b)
            print "Fetching %s of %s, %s" % (b, e, filename)
            try:
                f = open(filename, 'r')
                #import pdb;pdb.set_trace()
            except IOError:
                print "no such file.skipped"
                continue

            all_full = [eval(l)[1][1][0] for l in f.readlines()]
            if all_full == [] or all_full is None:
                print "empty data. skipped."
                continue

            avg_full = float(sum(all_full)) / len(all_full)

            error = calculate_error(all_full, avg_full)

            bench_data[b] = {}
            bench_data[b]["avg"] = avg_full / 1024**2
            bench_data[b]["err"] = error / 1024**2

            f.close()
        data[e] = bench_data
    return data

def read_speed(executables=executables):
    analysis = "speed"
    data = {}.fromkeys(executables)
    for e in executables:
        bench_data = {}
        for b in benchmarks:
            filename = "%s/speed_%s_%s_%s.csv" % (csv_folder, analysis, e, b)
            print "Fetching %s of %s, %s" % (b, e, filename)
            try:
                f = open(filename, 'r')
            except IOError:
                speed = "0"
		print "Couldn't read file. Setting speed to 0."
                continue

            all_full = [float(l) for l in f.readlines()]
            all_full = all_full[5:] # skip first 5 results

            if all_full == []:
                continue

            avg_full = float(sum(all_full)) / len(all_full)

            error = calculate_error(all_full, avg_full)

            bench_data[b] = {}
            bench_data[b]["avg"] = avg_full
            bench_data[b]["err"] = error

            f.close()
        data[e] = bench_data
    return data

def read_pypy_speed(executables=executables):
    data = {}.fromkeys(executables)
    for e in executables:

        bench_data = {}

        # get data from pypy-benchmarks dump
        f = open("pypy-benchmarks/speed_%s.json" % (e,),"r")
        dump = json.loads(f.read())
        results = dump['results']
        f.close()

        for l in results:
            # l = [<name>, RawResult, {"changed times": [...], "base times": [...]}]
            benchmark_name = l[0]
            times = l[2]["base_times"]
            times = times[5:] # skip first 5 results

            if len(times) == 0:
                continue
            avg_times = float(sum(times) / len(times))
            err_times = calculate_error(times, avg_times)

            bench_data[benchmark_name] = {}
            bench_data[benchmark_name]["avg"] = avg_times
            bench_data[benchmark_name]["err"] = err_times
        data[e] = bench_data
    return data

def read_transitions(executables=executables):
    data = {}.fromkeys(benchmarks)
    for b in benchmarks:
        data[b] = {}
        data[b]['list'] = {}
        data[b]['set'] = {}
        data[b]['dict'] = {}

        with file("%s/transitions_pypy-transitions_%s.csv" % (csv_folder, b)) as f:
            s = f.read()

        d = {}
        exec s in d
        d = d["strategytransitions"]

        for c in ['list','set','dict']:
            for t in ['int','str','float']:
                print c,t
                try:
                    allocated = sum(d['new %s_%s' % (t,c)].values())
                    to_obj = sum(d['%s_%s to object_%s' % (t,c,c)].values())
                    data[b][c][t] = {'allocated': allocated, 'to_obj': to_obj}
                except KeyError:
                    data[b][c][t] = {'allocated': 0, 'to_obj':0}
                data[b][c]['total allocated'] = data[b][c].get('total allocated',0) + data[b][c][t]['allocated']
    return data

def calculate_error(all_full, avg_full):
    # Standardabweichung berechnen
    n = max(len(all_full),2)
    x_ = avg_full
    c1 = float(1)/(n-1)
    c2 = sum([(x - x_)**2 for x in all_full])
    std = math.sqrt(c1 * c2)
    error = std * 1.959964
    return error
