from transition_diagram import read_and_join

def main():
    tr = read_and_join([])
    for kind, maxtagged in [("string", 7), ("unicode", 2)]:
        num_below, total = 0, 0
        for size, amount in tr["new %s" % kind].iteritems():
            if size <= maxtagged:
                num_below += amount
            total += amount
        print kind, "%s/%s (%s %%) can be tagged" % (num_below, total, float(num_below)/total)



if __name__ == '__main__':
    main()
