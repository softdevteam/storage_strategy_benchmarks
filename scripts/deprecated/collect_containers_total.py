import csv
import os

executables = ["python"]
analysis = "containers"

from reader import benchmarks

# ____________ HELPER FUNCS ____________

def get_container_content(typecategory, string):
    if typecategory == "dict":
        string = string.split(",")[0]

    contentamount = string.rpartition(":")[2][:-1]

    #contentstring = string.rpartition("[")[2]
    contentstring = string
    if contentstring.startswith(typecategory+"[str"):
        contenttype = "str"
    elif contentstring.startswith(typecategory+"[int"):
        contenttype = "int"
    elif contentstring.startswith(typecategory+"[float"):
        contenttype = "float"
    else:
        contenttype = "rest"

    return contenttype, contentamount

# ____________ IMPLEMENTATION _____________

#data = [["#"]]
#data[0].extend(executables)
alldata = []
for b in benchmarks:
    benchmark = b
    d = {"list":{}, "dict": {}, "set":{}}
    data_available = True
    for e in executables:
        filename = "memory-benchmarks/csv/%s_%s_%s.csv" % (analysis, e, b)
        print "Fetching %s of %s from %s" % (b, e, filename)
        try:
            csvfile = open(filename, 'r')
        except IOError:
            data_available = False
            break
        csvreader = csv.reader(csvfile, delimiter=",")
        csvdata = list(csvreader)
        csvfile.close()

        total_memory = 0
        for line in csvdata[2:]:
            if line[0] == "shutdown":
                continue

            typecategory = line[1]
            total_heapsize = line[3]
            total_memory = total_memory + int(total_heapsize)
            try:
                typedict = d[typecategory]
            except:
                continue;
            contenttype, contentamount = get_container_content(typecategory, line[2])
            oldsize = typedict.get(contenttype,0)
            typedict[contenttype] = oldsize + int(total_heapsize)
    if data_available:
        alldata.append((b, d, total_memory))

print alldata
alltypes = ["str", "int", "float", "rest"]
for container in ["list", "dict", "set"]:
    f = open("containers_"+container+".gpi", "w")
    f.write("""
        red = "#ff2626"
        green = "#2dc800"
        blue = "#2faace"
        purple = "#8d18ab"
        turkis = "#4fbddd"
        yellow = "#ffbe28"

        set size 1,0.8

        set key autotitle columnheader
        #set title "%s"
        set key invert outside
        set xtics nomirror rotate by -45
        set style histogram rowstacked
        set style fill solid border -1 #before: solid
        set style data histograms
        set boxwidth 0.7
        set yrange [0:1]
        set ylabel "Fraction of total memory usage"
        # define left/upper border
        #set style line 11 lc rgb '#808080' lt 1
        #set border 3 back ls 11
        #define grid
        set style line 12 lc rgb '#808080' lt 0 lw 1
        set grid noxtics ytics back ls 12
        #plot 'data/containers_.dat' using 2:xtic(1) lt rgb red, '' using 3 lt rgb green, '' using 4 lt rgb blue, '' using 5 lt rgb yellow
        plot 'data/containers_%s.dat' using 2:xtic(1) fs pattern 5, '' using 3 lt rgb "#000000", '' using 4 fs pattern 1, '' using 5 lt rgb "#ffffff"

        set terminal postscript eps monochrome solid #before: color
        set output "eps/containers_%s.eps"
        replot
    """ % (container, container, container))
    f = open("data/"+analysis+"_"+container+".dat", "w")
    # write header
    header = ["benchmark"]
    for t in alltypes:
        header.append(" ")
        header.append(t)
    f.write("".join(header))
    f.write("\n")

    for benchmark, data, mem in alldata:
        total = 0
        for v in data[container].values():
            total += float(v)

        if benchmark.endswith("strategy") or benchmark == "empty":
            benchmark += "*"
        f.write(benchmark)
        for typ in alltypes:
            try:
                size = float(data[container][typ])
                size = size / mem
            except KeyError:
                size = 0
            f.write(" %f" % size)
        f.write("\n")

    # write total average
    mean = {}
    for typ in alltypes:
        mean[typ] = []

    for benchmark, data, mem in alldata:
        for typ in alltypes:
            try:
                size = float(data[container][typ]) # total mem usage of typ in benchmark
            except KeyError:
                size = 0
            fraction = size / mem   # pecentage of typ in benchmark
            mean[typ].append(fraction)

    f.write("arith.mean")
    for typ in alltypes:
        total = sum(mean[typ]) / len(benchmarks)
        f.write(" %f" % (total))

    f.close()

    os.system("gnuplot containers_"+container+".gpi -persist")
    #os.system("epstopdf eps/containers_"+container+".eps")
    #os.system("cp eps/containers_"+container+".pdf /home/lukas/uni/memory_paper/images")

