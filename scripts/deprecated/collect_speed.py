import csv
import os

#executables = ["python", "default", "lists", "sets"]
executables = ["python", "pypy-default-noliststr32", "pypy-default-liststr32", "pypy-setstr32", "pypy-attributestr-32", "pypy-merged32-opt2"]
analysis = "osheapsize"
benchmarks = ["listbench", "networkx", "pylint", "scapy", "pyexcelerator",
              "pydblite", "pypy-interp", "pypy-translate","whoosh",
              "feedparser", "nltk-wordassoc", "nlp-decca-pos", "disaster",
              "bazaar", "multiwords", "liststrategy", "orm", "setstrategy",
              "invindex", "slowsets", "findprimes", "findprimes2"]

#data = [["#"]]
#data[0].extend(executables)
data = []
for b in benchmarks:
    bench_data = [b]
    for e in executables:
        print "Fetching %s of %s" % (b, e)
        filename = "memory-benchmarks/csv/speed_%s_%s_%s" % (analysis, e, b)
        try:
            f = open(filename, 'r')
            speed = f.read()
            speed = round(float(speed), 2)
            f.close()
        except IOError:
            speed = 0
        bench_data.append(str(speed))
    data.append(bench_data)


f = open("data/speed.dat", "w")

f.write("benchmark python nolist list set attribute merged\n")
for l in data:
    f.write(" ".join(l))
    f.write("\n")
f.close()

f = open("gpi/speed.gpi", "w")
f.write("""
    red = "#ff2626"
    green = "#2dc800"
    blue = "#2faace"
    purple = "#8d18ab"
    turkis = "#4fbddd"
    yellow = "#ffbe28"

    set size 1,0.8

    set key autotitle columnheader
    #set title "%s"
    set key invert out vert center top
    set xtics nomirror rotate by -45
    set style fill solid border -1
    set style data histograms
    set boxwidth 0.9
    #set yrange [0:200]
    set logscale y
    # define left/upper border
    #set style line 11 lc rgb '#808080' lt 1
    #set border 3 back ls 11
    # define grid
    set style line 12 lc rgb '#808080' lt 0 lw 1
    set grid noxtics ytics back ls 12
    plot 'data/speed.dat' using 2:xtic(1) lt rgb red, '' using 3 lt rgb green, '' using 4 lt rgb yellow, '' using 5 lt rgb blue, '' using 6 lt rgb purple, '' using 7 lt rgb turkis

    set terminal postscript eps color solid
    set output "eps/speed.eps"
    replot
"""
)
f.close()

os.system("gnuplot gpi/speed.gpi -persist")
os.system("cp data/speed.dat /home/lukas/uni/masterthesis/data/")
