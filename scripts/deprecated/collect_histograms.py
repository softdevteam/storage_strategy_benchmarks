import csv
import os

executables = ["python"]
analysis = "histograms"

# defindes which benchmarks to include into the graph
from reader import benchmarks

alldata = []
alltypes = set()
mean = {}

for b in benchmarks:
    benchmark = b
    d = {}
    data_available = True
    for e in executables:
        filename = "memory-benchmarks/csv/%s_%s_%s.csv" % (analysis, e, b)
        print "Fetching %s of %s from %s" % (b, e, filename)
        try:
            csvfile = open(filename, 'r')
        except IOError:
            data_available = False
            print "skipped"
            break
        csvreader = csv.reader(csvfile, delimiter=",")
        csvdata = list(csvreader)
        csvfile.close()

        for line in csvdata[2:]:
            if line[0] == "shutdown":
                continue

            typecategory = line[1]
            total_heapsize = line[3]
            percentage = line[6]
            oldpercentage = d.get(typecategory,0.0)
            d[typecategory] = oldpercentage + float(percentage)
            alltypes.add(typecategory)

            # collect data for total average
            l = mean.get(typecategory, [])
            l.append(float(percentage))
            mean[typecategory] = l
    if data_available:
        alldata.append((b, d))

f = open("data/"+analysis+".dat", "w")

# convert set to list to have a fixed order
alltypes = sorted(alltypes)

# write header
header = ["benchmark"]
for t in alltypes:
    header.append(" ")
    header.append(t)
f.write("".join(header))
f.write("\n")

for benchmark, data in alldata:
    if benchmark.endswith("strategy") or benchmark == "empty":
        benchmark += "*"
    f.write(benchmark)
    for typ in alltypes:
        try:
            percentage = data[typ]
        except KeyError:
            percentage = 0
        f.write(" %f" % (percentage))
    f.write("\n")

# write total average to file
f.write("arith.mean")
for typ in alltypes:
    total = sum(mean[typ]) / len(benchmarks)
    f.write(" %f" % (total))
f.close()

os.system("gnuplot histograms.gpi -persist")
#os.system("epstopdf eps/histograms.eps")
#os.system("cp eps/histograms.pdf /home/lukas/uni/memory_paper/images")
