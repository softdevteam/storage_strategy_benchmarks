import csv
import os

executables = ["pypy-none", "pypy-all"]
#executables = ["python", "pypy-default-noliststr32", "pypy-default-liststr32", "pypy-setstr32", "pypy-attributestr-32"]
analysis = "gclogs"

from reader import benchmarks

#data = [["#"]]
#data[0].extend(executables)
data = {}.fromkeys(executables)
for e in executables:
    bench_data = ["benchmark startup runtime\n"]
    for b in benchmarks:
        print "Fetching %s of %s" % (b, e)
        filename = "memory-benchmarks/csv/%s_%s_%s.csv" % (analysis, e, b)
        try:
            csvfile = open(filename, 'r')
        except:
            continue
        lines = csvfile.readlines()
        csvfile.close()

        if len(lines) == 0:
            linecount = 1
        else:
            linecount = len(lines)

        total = 0
        for l in lines:
            total += int(l)

        avg = total / linecount

        # convert Bytes to MB
        avg = avg / 1000000.0
        entry = "%s %s\n" %(b, round(avg,2))
        bench_data.append(entry)
    data[e] = bench_data

for e in executables:

    f = open("gclogs_"+ e +".gpi", "w")
    f.write("""
        red = "#ff2626"
        green = "#2dc800"
        blue = "#2faace"
        purple = "#8d18ab"
        turkis = "#4fbddd"
        yellow = "#ffbe28"

        set size 1,0.8

        set key autotitle columnheader
        set title "%s"
        set key invert outside
        set xtics nomirror rotate by -45
        set style histogram rowstacked
        set style fill solid border -1
        set style data histograms
        set boxwidth 0.7
        #set yrange [0:200]
        #set logscale y
        # define left/upper border
        #set style line 11 lc rgb '#808080' lt 1
        #set border 3 back ls 11
        # define grid
        set style line 12 lc rgb '#808080' lt 0 lw 1
        set grid noxtics ytics back ls 12
        plot 'data/gclogs_%s.dat' using 2:xtic(1) lt rgb red, '' using 3 lt rgb green

        set terminal postscript eps color solid
        set output "eps/gclogs_%s.eps"
        replot
    """ % (e, e, e))

    f = open("data/" + analysis + "_" + e + ".dat", "w")

    for l in data[e]:
        f.write("".join(l))
    f.close()

    os.system("gnuplot gclogs_"+e+".gpi -persist")
