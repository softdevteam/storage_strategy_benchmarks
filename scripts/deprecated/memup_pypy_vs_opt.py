import reader
import os

benchmarks = reader.benchmarks
data = reader.read_gclogs()

f = open("data/memup.dat", "w")

for b in benchmarks:
    try:
        pypynone = data["pypy-none"][b]['avg']
        if b.endswith("strategy") or b == "empty":
            f.write(b + "*")
        else:
            f.write(b)
    except KeyError:
        print "skipped", b
        continue
    try:
        pypydata = data["pypy-all"][b]['avg']
    except KeyError:
        print "skipped", b
        continue
    try:
        memup = float(pypydata) / float(pypynone)
    except ZeroDivisionError:
        memup = 0
    f.write(" %.2f" % memup)
    f.write("\n")
f.close()

f = open("gpi/memup.gpi", "w")
f.write("""
    red = "#ff2626"
    green = "#2dc800"
    blue = "#2faace"
    purple = "#8d18ab"
    turkis = "#4fbddd"
    yellow = "#ffbe28"

    set size 1,0.8

    #set key autotitle columnheader
    #set title "%s"
    set key invert out vert center top
    set xtics nomirror rotate by -45
    set style fill solid border -1
    set style data histograms
    set boxwidth 1#0.9
    #set logscale y
    set ylabel "Relative memory usage"
    set yrange [0:1.1]
    # define left/upper border
    #set style line 11 lc rgb '#808080' lt 1
    #set border 3 back ls 11
    # define grid
    set style line 12 lc rgb '#808080' lt 0 lw 1
    set grid noxtics ytics back ls 12
    set nokey
    plot 'data/memup.dat' using 2:xtic(1) lt rgb "#000000"

    set terminal postscript eps color solid
    set output "eps/memup.eps"
    replot
"""
)
f.close()

os.system("gnuplot gpi/memup.gpi -persist")
#os.system("epstopdf eps/memup.eps")
#os.system("cp eps/memup.pdf /home/lukas/uni/memory_paper/images/")
