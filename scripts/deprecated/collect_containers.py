import csv
import os

executables = ["python"]
analysis = "containers"
benchmarks = ["listbench", "networkx", "pylint", "scapy", "pyexcelerator",
              "pydblite", "pypy-interp", "pypy-translate","whoosh",
              "feedparser", "nltk-wordassoc", "nlp-decca-pos", "disaster",
              "bazaar", "multiwords", "liststrategy", "orm", "setstrategy",
              "invindex", "slowsets", "findprimes", "findprimes2"]

#benchmarks = ["bazaar","disaster","feedparser","multiwords", "networkx","pydblite", "pyexcelerator","pylint","scapy", "whoosh", "orm", "invindex"]

# ____________ HELPER FUNCS ____________

def get_container_content(typecategory, string):
    if typecategory == "dict":
        string = string.split(",")[0]

    contentamount = string.rpartition(":")[2][:-1]

    #contentstring = string.rpartition("[")[2]
    contentstring = string
    if contentstring.startswith(typecategory+"[str"):
        contenttype = "str"
    elif contentstring.startswith(typecategory+"[int"):
        contenttype = "int"
    elif contentstring.startswith(typecategory+"[float"):
        contenttype = "float"
    else:
        contenttype = "rest"

    return contenttype, contentamount

# ____________ IMPLEMENTATION _____________

#data = [["#"]]
#data[0].extend(executables)
alldata = []
for b in benchmarks:
    benchmark = b
    d = {"list":{}, "dict": {}, "set":{}}
    data_available = True
    for e in executables:
        print "Fetching %s of %s" % (b, e)
        filename = "memory-benchmarks/csv/%s_%s_%s.csv" % (analysis, e, b)
        try:
            csvfile = open(filename, 'r')
        except IOError:
            data_available = False
            break
        csvreader = csv.reader(csvfile, delimiter=",")
        csvdata = list(csvreader)
        csvfile.close()

        for line in csvdata[2:]:
            if line[0] == "shutdown":
                continue

            typecategory = line[1]
            try:
                typedict = d[typecategory]
            except:
                continue;
            contenttype, contentamount = get_container_content(typecategory, line[2])
            total_heapsize = line[3]
            oldsize = typedict.get(contenttype,0)
            typedict[contenttype] = oldsize + int(total_heapsize)
    if data_available:
        alldata.append((b, d))

print alldata
alltypes = ["str", "int", "float", "rest"]
for container in ["list", "dict", "set"]:
    f = open("containers_"+container+".gpi", "w")
    f.write("""
        red = "#ff2626"
        green = "#2dc800"
        blue = "#2faace"
        purple = "#8d18ab"
        turkis = "#4fbddd"
        yellow = "#ffbe28"

        set key autotitle columnheader
        #set title "%s"
        set key invert outside
        set xtics nomirror rotate by -45
        set style histogram rowstacked
        set style fill solid border -1
        set style data histograms
        set boxwidth 0.7
        plot 'data/containers_%s.dat' using 2:xtic(1) lt rgb red, '' using 3 lt rgb green, '' using 4 lt rgb blue, '' using 5 lt rgb yellow

        set terminal postscript eps color solid
        set output "eps/containers_%s.eps"
        replot
    """ % (container, container, container))
    f = open("data/"+analysis+"_"+container+".dat", "w")
    # write header
    header = ["benchmark"]
    for t in alltypes:
        header.append(" ")
        header.append(t)
    f.write("".join(header))
    f.write("\n")

    for benchmark, data in alldata:
        total = 0
        for v in data[container].values():
            total += float(v)

        f.write(benchmark)
        for typ in alltypes:
            try:
                size = float(data[container][typ])
                #size = size / total
                print size, total
            except KeyError:
                size = 0
            f.write(" %.3f" % size)
        f.write("\n")
    f.close()

    os.system("gnuplot containers_"+container+".gpi -persist")
    os.system("epstopdf eps/containers_"+container+".eps")
    os.system("cp eps/containers_"+container+".pdf /home/lukas/uni/masterthesis/images")

