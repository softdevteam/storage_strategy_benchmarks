import sys
import reader
import math

BM_MAP = {"networkx" : "NetworkX", "scapy" : "Scapy", "pyexcelerator" : "PyExcelerator",
  "pydblite" : "PyDbLite", "pypy-interp" : "PyPy-interp", "pypy-translate" : "PyPy-translate",
  "whoosh" : "whoosh", "feedparser" : "Feedparser", "nltk-wordassoc" : "nltk-wordassoc",
  "disaster" : "disaster", "multiwords" : "multiwords", "orm" : "orm",
  "findprimes" : "findprimes", "invindex" : "invindex", "slowsets" : "slowsets",
  "liststrategy" : "liststrategy*", "setstrategy" : "setstrategy*",
  "dictstrategy" : "dictstrategy*", "richards" : "richards", "slowspitfire" : "slowspitfire",
  "django" : "django", "spambayes" : "spambayes", "html5lib" : "html5lib", "ai" : "ai",
  "raytrace-simple" : "raytrace-simple", "crypto_pyaes" : "crypto\\_pyaes", "go" : "go",
  "chaos" : "chaos", "twisted_names" : "twisted\\_names", "meteor-contest" : "meteor-contest",
  "pyflate-fast" : "pyflate-fast", "telco" : "telco", "genshi_text" : "genshi\\_text", "nbody_modified" : "nbody\\_modified",
  "spectral-norm" : "spectral-norm", "sympy_integrate" : "sympy\\_integrate",
  "fannkuch" : "fannkuch", "bm_chameleon" : "bm\\_chameleon", "bm_mako" : "bm\\_mako"
}

# some helper functions
def std_dev_product(A, std_dev_A, B, std_dev_B):
    return math.sqrt((std_dev_A / A) ** 2 + (std_dev_B / B) ** 2) * A * B

def std_dev_quotient(A, std_dev_A, B, std_dev_B):
    return math.sqrt((std_dev_A / A) ** 2 + (std_dev_B / B) ** 2) * A / B

quantile = 1.959963984540

# read type of analysis and included executables from arguments
analysis = sys.argv[1] # "gclogs"
mode = sys.argv[2] # mode = latex,html
executables = sys.argv[3].split(",") #["pypy-all", "pypy-none"]

print "Analysis:"+analysis
print "Mode:"+mode
print "Executables:"+", ".join(executables)

# get ordered benchmark list
benchmarks = reader.benchmarks
benchmarks.remove('empty')
benchmarks.remove('findprimes')
benchmarks.remove('liststrategy')
benchmarks.remove('setstrategy')
benchmarks.remove('dictstrategy')
benchmarks.remove('pypy-interp')
benchmarks.remove('pypy-translate')
benchmarks.sort()
write_errors = True
percentages = False

# get data from csv files and read it into a dictionary
if analysis == "gclogs":
    data = reader.read_gclogs(executables)
    rounding = 1
    err_rounding = 0
    write_errors = False
elif analysis == "speed":
    data = reader.read_speed(executables)
    rounding = 3
    err_rounding = 2
elif analysis == "osheapsize":
    data = reader.read_osheapsize(executables)
    rounding = 1
    err_rounding = 1
    write_errors = False
elif analysis == "pypy-speed":
    data = reader.read_pypy_speed(executables)
    rounding = 3
    err_rounding = 2
elif analysis == "all-speed" or analysis == "all-speed-percentages":
    if analysis == "all-speed":
        rounding = 3
        err_rounding = 3
    else:
        rounding = 3
        err_rounding = 3

    speed = reader.read_speed(executables)
    pypyspeed = reader.read_pypy_speed(executables)

    pypy_benchmarks = "richards,slowspitfire,django,spambayes,html5lib,ai,raytrace-simple,crypto_pyaes,go,chaos,twisted_names,meteor-contest,pyflate-fast,telco,genshi_text,nbody_modified,spectral-norm,sympy_integrate,fannkuch,bm_chameleon,bm_mako".split(",")
    pypy_benchmarks.sort()
    benchmarks += pypy_benchmarks

    for e in executables:
        speed[e].update(pypyspeed[e])
    data = speed

if analysis == "all-speed-percentages":
    percentages = True

normalised_executable = executables[0]

# compile data into latex tables
#mode = "latex"

if mode == "latex":
    speedup = {}
    stddev_speedup = {}
    for e in executables:
        speedup[e] = 1
        stddev_speedup[e] = 0
    f = open("tables/%s.tex" % (analysis,), "w")
    f.write("\\addtolength{\\tabcolsep}{-2pt}\n")
    f.write("\\begin{tabular}")
    # column alignment
    f.write("{ l ")
    for e in executables:
        if write_errors:
            if percentages and e == "pypy-none":
                continue
                #f.write(" c")
            else:
                f.write(" >{\hspace{6pt}}rcl")
        else:
            f.write(" r")
    f.write("}\n")
    f.write("\\toprule\n")

    # executable row
    if write_errors:
        f.write("\emph{Benchmark}")
        for e in executables:
            if percentages and e == "pypy-none":
                continue
                #f.write(" & %s" % (e,))
            else:
                f.write(" & \multicolumn{3}{c}{\hspace{5pt}%s\hspace{5pt}}" % (e,))
        f.write("""\\\\\n""")
    else:
        f.write("benchmark")
        for e in executables:
            f.write(" & %s" % (e,))
        f.write("""\\\\\n""")
    f.write("\\midrule\n")

    # benchmark rows and results
    for b in benchmarks:
        if b == "ai":
            f.write("\\midrule\n")

        b_escape = b.replace('_','\_')
        f.write(BM_MAP[b])
        for e in executables:
            if percentages and e == "pypy-none":
                continue
            f.write(" & ")
            if data[e].has_key(b) and data['pypy-none'].has_key(b):
                value = float(data[e][b]['avg'])
                normalize_to = float(data['pypy-none'][b]['avg'])
                ratio = value / normalize_to
                std_dev_A = float(data[e][b]['err']) / quantile # undo confidence interval
                std_dev_B = float(data['pypy-none'][b]['err']) / quantile
                stddev_quotient = math.sqrt((std_dev_A / value) ** 2 + (std_dev_B / normalize_to) ** 2) * ratio
                old_speedup = speedup[e]
                speedup[e] *= ratio
                stddev_speedup[e] = std_dev_product(old_speedup, stddev_speedup[e], ratio, stddev_quotient)

                if percentages and e == "pypy-none":
                    formatstring = "%%0.%sf" % (1,)
                else:
                    formatstring = "%%0.%sf" % (rounding,)
                if not percentages:
                    f.write(formatstring % (value,))
                else:
                    if ratio > 1.0:
                        macro = "slower"
                    elif ratio == 1.0:
                        macro = "equal"
                    else:
                        macro = "faster"
                    f.write("\%s{%s}" % (macro, (formatstring % ratio)))
                if write_errors:
                    if percentages and e == "pypy-none":
                        pass # don't write error
                    else:
                        f.write(" & ")
                        formatstring = "\hspace{-6pt}\\tiny{$\pm$} & \hspace{-6pt}\\tiny{%%0.%sf}" % (err_rounding,)
                        if not percentages:
                            f.write(formatstring % (round(data[e][b]['err'], err_rounding),))
                        else:
                            f.write(formatstring % (round(stddev_quotient * quantile, err_rounding),))
            else:
                f.write("\multicolumn{3}{c}{-}")
        f.write(" \\\\\n")

    f.write("\\midrule\n")

    # write speedup row
    if analysis in ["osheapsize","gclogs"]:
        s = "Memory decrease"
    else:
        s = "Ratio"
    if write_errors:
        if percentages:
            f.write("\multicolumn{1}{l}{\emph{%s}}" % (s,))
        else:
            f.write("\multicolumn{4}{l}{\emph{%s}}" % (s,))
    else:
        f.write("\multicolumn{2}{l}{\emph{%s}}" % (s,))

    for e in executables:
        if e == "pypy-none":
            continue
        f.write(" & ")
        exponent = 1.0/len(benchmarks)
        percent = round(speedup[e] ** exponent,3)
        stddev_mean = exponent * stddev_speedup[e] * speedup[e] ** (exponent - 1)
        confidence = round(stddev_mean * quantile, 3)
        if analysis in ["osheapsize","gclogs"]:
            f.write("%s" % (percent,))
        else:
            formatstring = "%%0.%sf" % (rounding,)
            if percentages:
                f.write("\multicolumn{3}{>{\hspace{6pt}}c}{%s \\tiny{$\pm$ %s}}" % (formatstring % percent, formatstring % confidence))
            else:
                f.write("\multicolumn{3}{>{\hspace{6pt}}c}{%s \\tiny{$\pm$ %s}}" % (formatstring % percent, formatstring % confidence))

    f.write(" \\\\\n")
    f.write("\\bottomrule\n")
    f.write("""
\end{tabular}
""")

    f.close()

elif mode == "html":
    speedup = {}
    stddev_speedup = {}
    for e in executables:
        speedup[e] = 1
        stddev_speedup[e] = 0
    f = open("tables/%s.html" % (analysis,), "w")
    f.write("<html><body>\n")
    f.write("<table style='border-collapse:collapse'>\n")

    # executable row
    f.write("<tr style='border-bottom: 1px solid #000000'>")
    if write_errors:
        f.write("<td>Benchmarks</td>")
        for e in executables:
            if percentages and e == normalised_executable:
                continue
                #f.write(" & %s" % (e,))
            else:
                f.write("<td>%s</td>" % (e,))
    else:
        f.write("<td>benchmark</td>")
        for e in executables:
            f.write("<td>%s</td>" % (e,))
    f.write("</tr>\n")

    # benchmark rows and results
    for b in benchmarks:
        #if b == "ai":
        #    f.write("\\midrule\n")

        #b_escape = b.replace('_','\_')
        f.write("<tr>")
        f.write("<td>"+BM_MAP[b]+"</td>")
        for e in executables:
            if percentages and e == normalised_executable:
                continue
            f.write("<td align='right' style='padding-right: 10px'>")
            if data[e].has_key(b) and data[normalised_executable].has_key(b):
                value = float(data[e][b]['avg'])
                normalize_to = float(data[normalised_executable][b]['avg'])
                ratio = value / normalize_to
                std_dev_A = float(data[e][b]['err']) / quantile # undo confidence interval
                std_dev_B = float(data[normalised_executable][b]['err']) / quantile
                stddev_quotient = math.sqrt((std_dev_A / value) ** 2 + (std_dev_B / normalize_to) ** 2) * ratio
                old_speedup = speedup[e]
                speedup[e] *= ratio
                stddev_speedup[e] = std_dev_product(old_speedup, stddev_speedup[e], ratio, stddev_quotient)

                if percentages and e == normalised_executable:
                    formatstring = "%%0.%sf" % (1,)
                else:
                    formatstring = "%%0.%sf" % (rounding,)
                if not percentages:
                    f.write(formatstring % (value,))
                else:
                    if ratio > 1.0:
                        macro = "slower"
                    elif ratio == 1.0:
                        macro = "equal"
                    else:
                        macro = "faster"
                    f.write("%s" % ((formatstring % ratio)))
                if write_errors:
                    if percentages and e == normalised_executable:
                        pass # don't write error
                    else:
                        f.write(" &plusmn; ")
                        formatstring = "%%0.%sf" % (err_rounding,)
                        if not percentages:
                            f.write(formatstring % (round(data[e][b]['err'], err_rounding),))
                        else:
                            f.write(formatstring % (round(stddev_quotient * quantile, err_rounding),))
            else:
                f.write("-")
            f.write("</td>")
        f.write("</tr>\n")

    # write speedup row
    f.write("<tr>")
    if analysis in ["osheapsize","gclogs"]:
        s = "Memory decrease"
    else:
        s = "Ratio"
    if write_errors:
        if percentages:
            f.write("<td>%s</td>" % (s,))
        else:
            f.write("<td colspan=2>%s</td>" % (s,))
    else:
        f.write("<td colspan=2>%s</td>" % (s,))

    for e in executables:
        if e == normalised_executable:
            continue
        f.write("<td align='right'>")
        exponent = 1.0/len(benchmarks)
        percent = round(speedup[e] ** exponent,3)
        stddev_mean = exponent * stddev_speedup[e] * speedup[e] ** (exponent - 1)
        confidence = round(stddev_mean * quantile, 3)
        if analysis in ["osheapsize","gclogs"]:
            f.write("%s" % (percent,))
        else:
            formatstring = "%%0.%sf" % (rounding,)
            if percentages:
                f.write("%s &plusmn; %s" % (formatstring % percent, formatstring % confidence))
            else:
                f.write("%s &plusmn; %s" % (formatstring % percent, formatstring % confidence))

        f.write("</td>")
    f.write("</tr>\n")
    f.write("</table>\n</body></html>")

    f.close()
