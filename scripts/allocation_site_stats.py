from transition_diagram import read_and_join

histogram_divisions = 0.05
histogram_scaling = int(1.0 / histogram_divisions)
histogram_size = histogram_scaling + 2

def sanity_check(transitions, codesites):
    total_by_event1 = {}
    for name, items in transitions.iteritems():
        total_by_event1[name] = sum(items.values())

    total_by_event2 = {}
    for (a, b), items in codesites.iteritems():
        for event, count in items.iteritems():
            total_by_event2[event] = total_by_event2.get(event, 0) + count
    assert total_by_event1 == total_by_event2

def print_blub(transitions, codesites):
    hist = [0] * histogram_size
    threshold = 0.05
    total = {}
    for (a, b), items in codesites.iteritems():
        for event, count in items.iteritems():
            if event in ("new string", "new unicode"):
                continue
            d = total.setdefault(a, {})
            d[event] = d.get(event, 0) + count

    for a, d in total.iteritems():
        all = empty = spec = nspec = dehom = 0
        for event, count in d.iteritems():
            if event.startswith("new"):
                all += count
                if event.startswith("new empty"):
                    empty += count
                elif event.startswith("new object"):
                    nspec += count
                else:
                    spec += count
            elif " to " in event:
                if event.startswith("empty"):
                    empty -= count # does not stay empty
                if "to object" in event:
                    if event.startswith("empty"):
                        nspec += count
                    else:
                        dehom += count
                else:
                    if event.startswith("empty"):
                        spec += count
                    #else:
                        #print event, "is an oddball"
        if not all:
            continue
        f = float(dehom) / all
        hist[int(f * histogram_scaling) + (f != 0)] += 1
    for i, count in enumerate(hist):
        if i == 0:
            name = "0%"
        elif i == histogram_size - 1:
            name = "100%"
        else:
            name = "%s%% - <%s%%" % ((i - 1) * histogram_divisions, i * histogram_divisions)
        print "%s, %s, %s" % (name, count, float(count) / sum(hist))

    print
    print "num allocation sites,", len(total)



def main():
    import argparse

    parser = argparse.ArgumentParser(description='print stats about allocation sites')
    parser.add_argument('files', metavar='FILE', type=str, nargs='*',
                       help='all transition files to sum over')

    args = parser.parse_args()

    files = args.files

    transitions, codesites = read_and_join(files)
    sanity_check(transitions, codesites)
    print_blub(transitions, codesites)


if __name__ == '__main__':
    main()
