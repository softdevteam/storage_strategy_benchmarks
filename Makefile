# Executable to run buildprocess with. Use PyPy for faster buildtime.
PYTHON=python

PYPY_REVISION=341e1e3


all: builds/pypy-all builds/pypy-none builds/pypy-list builds/pypy-set builds/pypy-dict \
	  builds/pypy-strings builds/pypy-ints builds/pypy-floats builds/pypy-transitions
	./createvirtualenv.sh
	./benchmarks.sh --memory pypy-all osheapsize
	./benchmarks.sh --memory pypy-none osheapsize
	./benchmarks.sh --memory pypy-list osheapsize
	./benchmarks.sh --memory pypy-set osheapsize
	./benchmarks.sh --memory pypy-dict osheapsize
	./benchmarks.sh --memory pypy-ints osheapsize
	./benchmarks.sh --memory pypy-floats osheapsize
	./benchmarks.sh --memory pypy-strings osheapsize
	./benchmarks.sh --memory pypy-all speed
	./benchmarks.sh --memory pypy-none speed
	./benchmarks.sh --memory pypy-list speed
	./benchmarks.sh --memory pypy-set speed
	./benchmarks.sh --memory pypy-dict speed
	./benchmarks.sh --memory pypy-ints speed
	./benchmarks.sh --memory pypy-floats speed
	./benchmarks.sh --memory pypy-strings speed
	./benchmarks.sh --pypy pypy-all
	./benchmarks.sh --pypy pypy-none
	./benchmarks.sh --pypy pypy-list
	./benchmarks.sh --pypy pypy-set
	./benchmarks.sh --pypy pypy-dict
	./benchmarks.sh --pypy pypy-ints
	./benchmarks.sh --pypy pypy-floats
	./benchmarks.sh --pypy pypy-strings
	./benchmarks.sh --pypy-vs
	./benchmarks.sh --memory pypy-transitions transitions
	./benchmarks.sh --pypy pypy-transitions transitions
	./builddiagrams.sh

pypy-src:
	hg clone https://bitbucket.org/pypy/benchmarks pypy-benchmarks
	cd pypy-benchmarks && patch -p1 < ../diffs/correct_number_of_iterations.diff
	hg clone https://bitbucket.org/cfbolz/python-memory-benchmarks memory-benchmarks
	mkdir memory-benchmarks/csv
	wget https://bitbucket.org/cfbolz/python-memory-benchmarks/downloads/c1-una_small.xml -O memory-benchmarks/disaster/c1-una_small.xml
	hg clone -r ${PYPY_REVISION} https://bitbucket.org/pypy/pypy pypy-src

builds/pypy-none: pypy-src builds/pypy-all
	rm -rf pypy-none
	hg clone -r ${PYPY_REVISION} pypy-src pypy-none
	cd pypy-none && hg import --no-commit ../diffs/none.diff
	pypy-all/pypy-all pypy-none/pypy/translator/goal/translate.py --output=pypy-none-bin -Ojit \
	  pypy-none/pypy/translator/goal/targetpypystandalone.py --no-objspace-std-withliststrategies
	mv pypy-none-bin builds/pypy-none

builds/pypy-empty: pypy-src builds/pypy-all
	rm -rf pypy-empty
	hg clone -r ${PYPY_REVISION} pypy-src pypy-empty
	cd pypy-empty && hg import --no-commit ../diffs/empty.diff
	pypy-all/pypy-all pypy-empty/pypy/translator/goal/translate.py --output=pypy-empty-bin -Ojit \
	  pypy-empty/pypy/translator/goal/targetpypystandalone.py --no-objspace-std-withliststrategies
	mv pypy-empty-bin builds/pypy-empty

builds/pypy-list: pypy-src builds/pypy-all
	rm -rf pypy-list
	hg clone -r ${PYPY_REVISION} pypy-src pypy-list
	cd pypy-list && hg import --no-commit ../diffs/listonly.diff
	pypy-all/pypy-all pypy-list/pypy/translator/goal/translate.py --output=pypy-list-bin -Ojit
	mv pypy-list-bin builds/pypy-list

builds/pypy-set: pypy-src builds/pypy-all
	rm -rf pypy-set
	hg clone -r ${PYPY_REVISION} pypy-src pypy-set
	cd pypy-set && hg import --no-commit ../diffs/setsonly.diff
	pypy-all/pypy-all pypy-set/pypy/translator/goal/translate.py --output=pypy-set-bin -Ojit \
	  pypy-set/pypy/translator/goal/targetpypystandalone.py --no-objspace-std-withliststrategies
	mv pypy-set-bin builds/pypy-set

builds/pypy-dict: pypy-src builds/pypy-all
	rm -rf pypy-dict
	hg clone -r ${PYPY_REVISION} pypy-src pypy-dict
	cd pypy-dict && hg import --no-commit ../diffs/dictsonly.diff
	pypy-all/pypy-all pypy-dict/pypy/translator/goal/translate.py --output=pypy-dict-bin -Ojit \
	  pypy-dict/pypy/translator/goal/targetpypystandalone.py --no-objspace-std-withliststrategies
	mv pypy-dict-bin builds/pypy-dict

builds/pypy-all: pypy-src
	rm -rf pypy-all
	hg clone -r ${PYPY_REVISION} pypy-src pypy-all
	${PYTHON} pypy-all/pypy/translator/goal/translate.py --output=pypy-all-bin -Ojit
	cp pypy-all-bin pypy-all/pypy-all
	mv pypy-all-bin builds/pypy-all

builds/pypy-strings: pypy-src builds/pypy-all
	rm -rf pypy-strings
	hg clone -r ${PYPY_REVISION} pypy-src pypy-strings
	cd pypy-strings && hg import --no-commit ../diffs/stringsonly.diff
	pypy-all/pypy-all pypy-strings/pypy/translator/goal/translate.py --output=pypy-strings-bin -Ojit
	mv pypy-strings-bin builds/pypy-strings

builds/pypy-ints: pypy-src builds/pypy-all
	rm -rf pypy-ints
	hg clone -r ${PYPY_REVISION} pypy-src pypy-ints
	cd pypy-ints && hg import --no-commit ../diffs/intsonly.diff
	pypy-all/pypy-all pypy-ints/pypy/translator/goal/translate.py --output=pypy-ints-bin -Ojit
	mv pypy-ints-bin builds/pypy-ints

builds/pypy-floats: pypy-src builds/pypy-all
	rm -rf pypy-floats
	hg clone -r ${PYPY_REVISION} pypy-src pypy-floats
	cd pypy-floats && hg import --no-commit ../diffs/floatsonly.diff
	pypy-all/pypy-all pypy-floats/pypy/translator/goal/translate.py --output=pypy-floats-bin -Ojit
	mv pypy-floats-bin builds/pypy-floats

builds/pypy-transitions: pypy-src builds/pypy-all
	rm -rf pypy-transitions
	hg clone -r ${PYPY_REVISION} pypy-src pypy-transitions
	cd pypy-transitions && hg import --no-commit ../diffs/count_transitions.diff
	# -Ojit enables the options that the JIT would enable
	# --no-translation-jit disables the JIT itself
	pypy-all/pypy-all pypy-transitions/pypy/translator/goal/translate.py \
	  --output=pypy-transitions-bin -Ojit --no-translation-jit
	mv pypy-transitions-bin builds/pypy-transitions

clean:
	rm -f builds/pypy-src builds/pypy-none builds/pypy-list builds/pypy-set \
	  builds/pypy-dict builds/pypy-strings builds/pypy-ints builds/pypy-floats \
	  builds/pypy-transitions \
	  pypy-src pypy-none pypy-list pypy-set pypy-dict pypy-strings pypy-ints pypy-floats \
	  pypy-transitions
