#!/bin/sh

echo "BULDING DIAGRAMS"
echo "----------------"

python scripts/write_latextable.py osheapsize latex pypy-none,pypy-all,pypy-list,pypy-set,pypy-dict,pypy-ints,pypy-strings,pypy-floats
python scripts/write_latextable.py all-speed latex pypy-none,pypy-all,pypy-list,pypy-set,pypy-dict,pypy-ints,pypy-strings,pypy-floats
python scripts/write_latextable.py all-speed-percentages latex pypy-none,pypy-all,pypy-list,pypy-set,pypy-dict,pypy-ints,pypy-strings,pypy-floats

python scripts/transition_diagram.py > out.dot
dot -Tpdf out.dot > diagrams/transitions.pdf
rm out.dot
