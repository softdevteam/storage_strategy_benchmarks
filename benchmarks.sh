#!/bin/sh

case "$1" in
    --analysis|-a)
        BENCHMARK="analysis"
        ;;
    --memory|-m)
        BENCHMARK="memory"
        EXECUTABLE=$2
        TYPE=$3
        ;;
    --pypy|-p)
        BENCHMARK="pypy"
        EXECUTABLE=$2
        ;;
    --pypy-vs|-v)
        BENCHMARK="pypy-vs"
        ;;
    *)
        echo "You can pass the following options:"
        echo "  --analysis|-a"
        echo "  --memory|-m [EXECUTABLE] [TYPE] # runs memory benchmarks"
        echo "  --pypy|-p  [EXECUTABLE]         # runs PyPy benchmarks"
        echo "  --pypy-vs|-v                    # runs PyPy(all) vs PyPy(none)"
        exit 0
        ;;
esac

if [ $BENCHMARK = "analysis" ]; then

    cd memory-benchmarks
    mkdir csv

    echo "===> Preparing data type anysis using python"
    . ../pythonenv/bin/activate
    echo "     ===> Running osheapsize analysis"
    python newrunner.py -a osheapsize
    echo "     ===> Running container analysis"
    python newrunner.py -a containers
    echo "     ===> Running histograms analysis"
    python newrunner.py -a histograms

elif [ $BENCHMARK = "memory" ]; then

    if [ $EXECUTABLE = "pypy-transitions" ]; then
        ITERATIONS=1
    elif [ $TYPE = "gclogs" ] || [ $TYPE = "osheapsize" ]; then
        ITERATIONS=1
    else
        ITERATIONS=35
    fi

    echo "===> Running memory benchmarks using executable $EXECUTABLE with anaylsis $TYPE"
    cd memory-benchmarks
    . ../pypyenv/bin/activate
    $EXECUTABLE newrunner.py -n $ITERATIONS -a $TYPE -v

elif [ $BENCHMARK = "pypy" ]; then

    EXECUTABLE=$2
    cd pypy-benchmarks
    . ../pypyenv/bin/activate

    if [ $EXECUTABLE = "pypy-transitions" ]; then
        for B in richards slowspitfire django spambayes html5lib ai raytrace-simple crypto_pyaes go chaos twisted_names meteor-contest pyflate-fast telco genshi_text nbody_modified spectral-norm sympy_integrate fannkuch bm_chameleon bm_mako
        do
            echo "===> Running PyPy benchmark $B with $EXECUTABLE"
            $EXECUTABLE runner.py --fast -b $B
            cp strategytransitions.txt ../memory-benchmarks/csv/transitions_pypy-transitions_$B.csv
        done
    else

        echo "===> Running PyPy benchmarks with $EXECUTABLE"
        $EXECUTABLE runner.py -o speed_$EXECUTABLE.json --full-store -b richards,slowspitfire,django,spambayes,html5lib,ai,raytrace-simple,crypto_pyaes,go,chaos,twisted_names,meteor-contest,pyflate-fast,telco,genshi_text,nbody_modified,spectral-norm,sympy_integrate,fannkuch,bm_chameleon,bm_mako
    fi

elif [ $BENCHMARK = "pypy-vs" ]; then

    cd pypy-benchmarks
    . ../pypyenv/bin/activate

    echo "==> Running PyPy benchmarks (pypy-none against pypy)"
    pypy-none runner.py -c pypy-all --full-store -o speed_vs.json -b richards,slowspitfire,django,spambayes,html5lib,ai,raytrace-simple,crypto_pyaes,go,chaos,twisted_names,meteor-contest,pyflate-fast,telco,genshi_text,nbody_modified,spectral-norm,sympy_integrate,fannkuch,bm_chameleon,bm_mako

fi
